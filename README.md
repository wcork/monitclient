# Monit Client

Java client to interface with the web daemon of Monit.

### Usage
```Java
//User and pass are optional
MonitClient client = new MonitClient("http://localhost:2812", "admin", "monit");
Map<String, MonitService> services = client.getServices();

MonitService myService = services.get("MY SERVICE");
myService.unmonitor();

// Updating the client will update all services from remote server.
client.update();
```

### Building
Use the gradle wrapper:

*Windows*:

`gradlew.bat jar`

**nixx*:

`./gradlew jar`

### Running tests

Tests assume that you are running a Monit web daemon on the local machine with 
default port and user:pass as admin:monit. Tests will fail unless it is 
running.

`./gradlew test`

Show output:

`./gradlew test -i`

### To-do
* More testing
* Add more service attributes to MonitService obj as members
* Translate attribute values as `enum` values for easier filtering.

