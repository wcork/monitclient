import cxro.common.monit.MonitClient;
import cxro.common.monit.MonitService;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class MonitTest
{
  @Test
  public void ConnectionTest()
  {
    try {
      MonitClient client = new MonitClient("http://localhost:2812", "admin", "monit");
      Map<String, MonitService> services = client.getServices();

      System.out.println("Connected to " + client.getHost());

      for (Object o : services.entrySet()) {
        Map.Entry entry = (Map.Entry) o;
        System.out.println("Monitored?    " + ((MonitService)entry.getValue()).isMonitored());
        System.out.println(entry.getValue());
      }


    } catch (Exception e) {
      System.out.println(e);
      fail();
    }
  }

  @Test
  public void MonitorActionTest()
  {
    try {
      MonitClient client = new MonitClient("http://localhost:2812", "admin", "monit");
      Map<String, MonitService> services = client.getServices();

      for (Object o : services.entrySet()) {
        Map.Entry entry = (Map.Entry) o;
        System.out.println(entry.getValue());
      }

      services.entrySet().stream().filter(s -> s.getValue().getType() == MonitService.SERVICE_TYPE.SYSTEM)
          .findFirst().ifPresent(sysService -> {
        try {
          System.out.println("Testing \'unmonitor\' action");
          sysService.getValue().unmonitor();
          client.update(true);
          assertEquals("0", sysService.getValue().getAttributes().get("monitor"));

          System.out.println("Monitored?    " + ((MonitService)sysService.getValue()).isMonitored());
          System.out.println(sysService.getValue());

          System.out.println("Testing \'monitor\' action");
          sysService.getValue().monitor();
          client.update(true);
          assertEquals("1", sysService.getValue().getAttributes().get("monitor"));

          System.out.println("Monitored?    " + ((MonitService)sysService.getValue()).isMonitored());
          System.out.println(sysService.getValue());

        } catch (Exception e) {
          System.out.println(e);
          fail();
        }
      });


    } catch (Exception e) {
      System.out.println(e);
      fail();
    }
  }

  @Test
  public void CompareTest()
  {
    try {
      MonitClient client = new MonitClient("http://localhost:2812", "admin", "monit");
      Map<String, MonitService> servicesBefore;
      Map<String, MonitService> servicesAfter;

      servicesBefore = new HashMap<>(client.getServices());
      client.update(true);
      servicesAfter = new HashMap<>(client.getServices());
      if (!servicesAfter.equals(servicesBefore)) {
        fail();
      }

      servicesBefore = new HashMap<>(client.getServices());
      servicesBefore.entrySet().stream().filter(s -> s.getValue().getType() == MonitService.SERVICE_TYPE.SYSTEM)
          .findFirst().ifPresent(sysService -> {
        try {
          sysService.getValue().unmonitor();
        } catch (Throwable t) {
          t.printStackTrace();
          fail();
        }
      });
      client.update(true);

      servicesAfter = new HashMap<>(client.getServices());
      if (servicesAfter.equals(servicesBefore)) {
        fail();
      }

      servicesAfter.entrySet().stream().filter(s -> s.getValue().getType() == MonitService.SERVICE_TYPE.SYSTEM)
          .findFirst().ifPresent(sysService -> {
        try {
          sysService.getValue().monitor();
        } catch (Throwable t) {
          t.printStackTrace();
          fail();
        }
      });


      client.update(true);
      servicesBefore = new HashMap<>(client.getServices());
      client.update(true);
      servicesAfter = new HashMap<>(client.getServices());
      if (!servicesAfter.equals(servicesBefore)) {
        fail();
      }

    } catch (Throwable t) {
      System.out.println(t);
      fail();
    }
  }

  @Test
  public void LoseServiceTest()
      throws IOException
  {
    MonitClient client = new MonitClient("http://localhost:2812", "admin", "monit");
    Map<String, MonitService> servicesBefore;
    Map<String, MonitService> servicesAfter;

    servicesBefore = new HashMap<>(client.getServices());
    // TODO: PUT BREAKPOINT HERE. REMOVE A MONIT SERVICE
    client.update(true);
    servicesAfter = new HashMap<>(client.getServices());

    System.out.println("Counts:");
    System.out.println("Before: " + servicesBefore.entrySet().size());
    System.out.println("After: " + servicesAfter.entrySet().size());
  }
}
