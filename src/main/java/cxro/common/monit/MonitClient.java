package cxro.common.monit;

import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * MonitClient
 * Used to interface with Monit Daemon web webserver.
 * Can monitor and send Monit actions.
 *
 * @author William Cork
 * created on 2019-03-29
 */

public class MonitClient
{
  URL baseUrl = null;
  Response updateResponse;
  Request httpRequest = null;
  OkHttpClient httpClient = null;

  private Request.Builder httpRequestBuilder = null;
  private HashMap<String, MonitService> monitServices = new HashMap<>();
  private int pollRate;

  /**
   * Creates a MonitClient obj.
   * Does initial connection to serverUrl
   *
   * @param serverUrl URL of Monit http server. Ex. http://localhost:2812
   * @throws IOException when any communication, interruption, or parsing exception occurs
   */
  public MonitClient(String serverUrl)
      throws IOException
  {
    this.baseUrl = new URL(serverUrl);
    this.setupHttp(serverUrl);
    this.httpClient = new OkHttpClient();
    this.httpRequest = httpRequestBuilder.build();
    this.update();
  }

  /**
   * Creates a MonitClient obj.
   * Does initial connection to serverUrl.
   * Will use BasicAuth from given user and pass.
   *
   * @param serverUrl URL of Monit http server. Ex. http://localhost:2812
   * @param user      BasicAuth user name
   * @param pass      BasicAuth password
   * @throws IOException when any communication, interruption, or parsing exception occurs
   */
  public MonitClient(String serverUrl, String user, String pass)
      throws IOException
  {
    this.baseUrl = new URL(serverUrl);
    this.setupHttp(serverUrl);
    this.httpClient = new OkHttpClient.Builder().authenticator((route, response) ->
        response.request().newBuilder().header("Authorization", Credentials.basic(user, pass)).build()).build();
    this.httpRequest = httpRequestBuilder.build();
    this.update();
  }

  /**
   * Constructor helper.
   *
   * @param serverUrl URL of Monit http server. Ex. http://localhost:2812
   */
  private void setupHttp(String serverUrl)
      throws MalformedURLException
  {
    URL targetUrl = new URL(serverUrl + "/_status?format=xml");
    httpRequestBuilder = new Request.Builder()
        .url(targetUrl.toString())
        .get();
  }

  /**
   * Gives all services attached to client.
   *
   * @return Services as the hashmap [ServiceName, MonitService]
   */
  public HashMap<String, MonitService> getServices()
  {
    return this.monitServices;
  }

  /**
   * @return Poll rate of Monit server in Seconds
   */
  public int getPollRateSeconds() {
    return this.pollRate;
  }

  public String getHost() {
    return this.baseUrl.toString();
  }

  /**
   * Updates the services map by querying the Monit Server.
   * Will only create new objects in map when they did not exist previously.
   *
   * @param waitForPending Setting true will poll until all pending actions on the Monit server are complete.
   * @throws IOException when any communication, interruption, or parsing exception occurs
   */
  public void update(boolean waitForPending)
      throws IOException
  {
    try {
      updateResponse = httpClient.newCall(httpRequest).execute();

      if (updateResponse.code() != 200) {
        throw new IOException("Invalid request: " + updateResponse.code());
      }
      if (updateResponse.body() == null) {
        throw new IOException("Invalid response body");
      }

      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      Document document = builder.parse(new InputSource(new StringReader(updateResponse.body().string())));

      // Place server poll rate in attributes
      NodeList serverList = document.getElementsByTagName("poll");
      if (serverList.getLength() < 1) {
        throw new IOException("No server tag in XML. Cannot find");
      } else {
        Node pollNode = serverList.item(0);
        this.pollRate = Integer.parseInt(pollNode.getTextContent());
      }

      NodeList serviceList = document.getElementsByTagName("service");
      List<String> serviceNameList = new ArrayList<>();
      for (int i = 0; i < serviceList.getLength(); i++) {
        Node node = serviceList.item(i);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          HashMap<String, String> serviceAttributes = new HashMap<>();

          Element typeElement = (Element) node;
          int serviceTypeVal = Integer.parseInt(typeElement.getAttribute("type"));
          if (MonitService.SERVICE_TYPE_MAP.keySet().contains(serviceTypeVal)) {
            serviceAttributes.put("type", MonitService.SERVICE_TYPE_MAP.get(serviceTypeVal).name());
          } else {
            serviceAttributes.put("type", MonitService.SERVICE_TYPE_MAP.get(-1).name());
          }

          NodeList serviceNodes = node.getChildNodes();
          for (int x = 0; x < serviceNodes.getLength(); x++) {
            Node serviceNode = serviceNodes.item(x);
            // TODO: Handle nested attributes
            // We skip system for now
            if (serviceNode.getNodeName().equals("system")) {
              continue;
            }

            // Handle pending actions
            if (waitForPending) {
              if (serviceNode.getNodeName().equals("pendingaction") && !serviceNode.getTextContent().equals("0")) {
                // Short circuit recursion for pending actions.
                Thread.sleep(1000);
                this.update(true);
                return;
              }
            }

            serviceAttributes.put(serviceNode.getNodeName(), serviceNode.getTextContent());
          }

          // Bookkeeping list
          serviceNameList.add(serviceAttributes.get("name"));
          // We need to place new objects.
          monitServices.remove(serviceAttributes.get("name"));
          monitServices.put(serviceAttributes.get("name"),
              new MonitService(this, serviceAttributes.get("name"), serviceAttributes));
        }
      }

      // We remove any services that are not in the serviceList XML
      List<String> oldServiceList = new ArrayList<>(this.monitServices.keySet());
      oldServiceList.removeAll(serviceNameList);
      oldServiceList.forEach(serviceName -> this.monitServices.remove(serviceName));

    } catch (InterruptedException | ParserConfigurationException | SAXException ex) {
      throw new IOException(ex);
    }
  }

  /**
   * Updates the services map by querying the Monit Server.
   * Will only create new objects in map when they did not exist previously.
   * Does not wait for pending actions to complete.
   * Same as calling {@link #update(boolean) update(false)}
   *
   * @throws IOException when any communication, interruption, or parsing exception occurs
   */
  public void update()
      throws IOException
  {
    this.update(false);
  }
}
