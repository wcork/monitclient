package cxro.common.monit;

import okhttp3.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Data store for Monit services.
 *
 * @author William Cork
 */
public class MonitService
{
  /**
   * A  useful map for converting from the Integer value of an enum to an enum
   */
  public static HashMap<Integer, SERVICE_TYPE> SERVICE_TYPE_MAP = new HashMap<>();

  static {
    SERVICE_TYPE_MAP.put(SERVICE_TYPE.FILESYSTEM.getValue(), SERVICE_TYPE.FILESYSTEM);
    SERVICE_TYPE_MAP.put(SERVICE_TYPE.DIRECTORY.getValue(), SERVICE_TYPE.DIRECTORY);
    SERVICE_TYPE_MAP.put(SERVICE_TYPE.FILE.getValue(), SERVICE_TYPE.FILE);
    SERVICE_TYPE_MAP.put(SERVICE_TYPE.PROCESS.getValue(), SERVICE_TYPE.PROCESS);
    SERVICE_TYPE_MAP.put(SERVICE_TYPE.CONNECTION.getValue(), SERVICE_TYPE.CONNECTION);
    SERVICE_TYPE_MAP.put(SERVICE_TYPE.SYSTEM.getValue(), SERVICE_TYPE.SYSTEM);
    SERVICE_TYPE_MAP.put(SERVICE_TYPE.UNKNOWN.getValue(), SERVICE_TYPE.UNKNOWN);
  }

  /**
   * Types of services stored in the 'type' attribute.
   * They are mapped to Monit service types.
   */
  public enum SERVICE_TYPE
  {
    FILESYSTEM(0),
    DIRECTORY(1),
    FILE(2),
    PROCESS(3),
    CONNECTION(4),
    SYSTEM(5),
    UNKNOWN(-1);

    private int value;

    SERVICE_TYPE(int i)
    {
      this.value = i;
    }

    public int getValue()
    {
      return this.value;
    }
  }

  private MonitClient client;
  private String name;
  private HashMap<String, String> attributes;
  private URL url;
  private SERVICE_TYPE type = SERVICE_TYPE.UNKNOWN;
  private int status;
  private int pendingAction;
  private boolean isMonitored;

  /**
   * Constructor for the data store
   *
   * @param client     The client that created the service service data store
   * @param name       The name of the service. Usually given by the parent client.
   * @param attributes Initial attributes. Usually given by the parent client
   * @throws MalformedURLException if created URL is malformed (should never happen)
   */
  public MonitService(MonitClient client, String name, HashMap<String, String> attributes)
      throws MalformedURLException
  {
    this.client = client;
    this.name = name;
    this.attributes = attributes;
    this.url = new URL(this.client.baseUrl + "/" + this.name);
    this.updateAttributes();
  }

  /**
   * @return The name of the service
   */
  public String getName() {
    return this.name;
  }

  /**
   * @return The status of the service
   */
  public int getStatus() {
    return this.status;
  }

  /**
   * @return true if the service is reported as running
   */
  public boolean isRunning() {
    return (this.type == SERVICE_TYPE.PROCESS) && (this.attributes.get("pid") != null);
  }

  /**
   * @return The pending action of the service.
   */
  public int getPendingAction() {
    return this.pendingAction;
  }

  /**
   * @return true if Monit is currently monitoring the service
   */
  public boolean isMonitored() {
    return this.isMonitored;
  }

  /**
   * @return all attributes of the MonitService as a HashMap
   */
  public HashMap<String, String> getAttributes()
  {
    return this.attributes;
  }

  /**
   * Allows setting the attributes of a Monit Service.
   * This is not a way to update a service.
   *
   * @param attributes assigns attributes to MonitService
   */
  void updateAttributes(HashMap<String, String> attributes)
  {
    this.attributes = new HashMap<>(attributes);
    this.updateAttributes();
  }

  /**
   * The 'type' attribute represented as an enum
   *
   * @return a SERVICE_TYPE enum of the service.
   */
  public SERVICE_TYPE getType()
  {
    return this.type;
  }

  /**
   * Sends the 'start' action to the service.
   *
   * @throws IOException
   */
  public void start()
      throws IOException
  {
    this.action("start");
  }

  /**
   * Sends a 'stop' action to the service.
   *
   * @throws IOException
   */
  public void stop()
      throws IOException
  {
    this.action("stop");
  }

  /**
   * Sends 'restart' action to the service.
   *
   * @throws IOException
   */
  public void restart()
      throws IOException
  {
    this.action("restart");
  }

  /**
   * Sends 'monitor' action to service.
   *
   * @throws IOException
   */
  public void monitor()
      throws IOException
  {
    this.action("monitor");
  }

  /**
   * Sends 'unmonitor' action to service.
   *
   * @throws IOException
   */
  public void unmonitor()
      throws IOException
  {
    this.action("unmonitor");
  }

  private void updateAttributes() {
    this.type = SERVICE_TYPE.valueOf(this.attributes.get("type"));
    this.status = Integer.parseInt(attributes.get("status"));
    this.pendingAction = Integer.parseInt(attributes.get("pendingaction"));
    this.isMonitored = attributes.get("monitor").equals("1");
  }

  /**
   * Action helper.
   *
   * @param action the action to send to the Monit http server.
   * @throws IOException
   */
  private synchronized void action(String action)
      throws IOException
  {
    // Make post action with payload
    Request.Builder actionRequestBuilder = new Request.Builder()
        .post(RequestBody.create(MediaType.parse("text"), ""));

    // for composing URL params
    StringBuilder actionData = new StringBuilder();

    // Grab cookies from update response
    String cookieHeader = this.client.updateResponse.headers().get("Set-Cookie");
    if (cookieHeader != null) {
      Cookie oCookie = Cookie.parse(this.client.updateResponse.request().url(), cookieHeader);
      if (oCookie != null) {
        StringBuilder sb = new StringBuilder().append(oCookie.name()).append("=").append(oCookie.value());
        actionData.append(sb.toString()).append("&");
        actionRequestBuilder = actionRequestBuilder.addHeader("Cookie", sb.toString());
      }
    }

    actionData.append("action").append("=").append(action);
    actionRequestBuilder = actionRequestBuilder.url(this.url.toString()).post(RequestBody.create(
        MediaType.parse("text"), actionData.toString()));
    Response res = this.client.httpClient.newCall(actionRequestBuilder.build()).execute();
    if (res.code() != 200) {
      throw new IOException("Unable to POST action " + action + " code: " + res.code());
    }
  }

  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder();
    sb.append("Service : ").append(this.name).append("\n");
    for (Object entry : this.attributes.entrySet()) {
      sb.append("    ").append(((Map.Entry) entry).getKey()).append(": ").append(((Map.Entry) entry).getValue()).append("\n");
    }
    return sb.toString();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }

    if (!MonitService.class.isAssignableFrom(obj.getClass())) {
      return false;
    }

    final MonitService other = (MonitService) obj;
    return this.attributes.equals(other.attributes);

  }
}
